'use strict';

angular.module('yapp')

.factory('userSession', function() {
    return {}
})

.factory("Author", function($firebaseArray, $firebaseObject, userSession) {

    var authorRef = new Firebase("https://library-qualit.firebaseio.com/");
    return {
        all: function() {
            return $firebaseArray(needsRef.child('authors'));
        },
        getAuthors: function(needsReady) {
            var needs = [];
            needsRef.child('needs').orderByChild("uid").equalTo(userSession.user.id).on("child_added", function(needSnap) {
                needsRef.child('users/facebook:' + needSnap.val().uid).on('value', function(userSnap) {
                    var completeNeed = needSnap.val().need;
                    completeNeed.user = userSnap.val().user;
                    completeNeed.id = needSnap.key();
                    needsReady(completeNeed);
                });
            });

        },
        remove: function(author, removeAuthorReady) {
            var authorToRemove = $firebaseObject(authorRef.child('authors/' + author.id));
            authorToRemove.$remove().then(function(ref) {
                // data has been deleted locally and in the database
                removeAuthorReady(author);
            }, function(error) {
                console.log("Error:", error);
            });

        }

    };
})

.factory("Book", function($firebaseArray, $firebaseObject, userSession) {
    var Ref = new Firebase("https://library-qualit.firebaseio.com/");
    return {
        all: function() {
            return $firebaseArray(needsRef.child('needswiped'));
        },
        remove: function(book, removeBookReady) {
            var bookToRemove = $firebaseObject(Ref.child('books/' + book.id));
            bookToRemove.$remove().then(function(ref) {
                removeBookReady(book);
            }, function(error) {
                console.log("Error:", error);
            });

        }

    };
});
