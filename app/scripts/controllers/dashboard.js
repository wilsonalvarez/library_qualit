'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
    .controller('DashboardCtrl', function($scope, $state, userSession) {
        $scope.name = "Invitado";

        if (typeof userSession.name != 'undefined' && userSession.name != null) {

            $scope.name = userSession.name;
        }

        $scope.$state = $state;

    })
    .controller('AuthorCtrl', function($scope, $state, Author, $firebaseArray) {
        $scope.authors = [];

        var messagesRef = new Firebase("https://library-qualit.firebaseio.com/authors/");
        messagesRef.on("child_added", function(authorSnap) {
            var completeAuthor = authorSnap.val();
            completeAuthor.id = authorSnap.key();
            $scope.authors.push(completeAuthor);

            $scope.$apply();

        });


        $scope.addAuthor = function() {
            var authorsRef = new Firebase("https://library-qualit.firebaseio.com/authors/");
            var authorRef = authorsRef.push();
            var NewA = { 'name': $scope.newAuthor.name, 'nationality': $scope.newAuthor.nationality, 'done': false };
            authorRef.set(NewA);

            $scope.newAuthor.name = '';
            $scope.newAuthor.nationality = '';
        }

        $scope.deleteAuthor = function(author) {

            Author.remove(author, function(authorDelete) {
                $scope.authors.splice($scope.authors.indexOf(authorDelete), 1);
            });
        }
    })


.controller('BooksCtrl', function($scope, $state, Author, Book, $firebaseArray) {
    $scope.addBookPanel = false;
    $scope.books = [];
    $scope.authors = [];
    var book = {};


    $scope.loadAuthors = function() {
        var messagesRef = new Firebase("https://library-qualit.firebaseio.com/authors/");
        messagesRef.on("child_added", function(authorSnap) {
            var completeAuthor = {};
            completeAuthor.value = authorSnap.key();
            completeAuthor.label = authorSnap.val().name + " " + authorSnap.val().nationality;
            $scope.authors.push(completeAuthor);
            $scope.$apply();

        });
    }
    $scope.loadAuthors();

    $scope.openAddBook = function() {
        $scope.addBookPanel = true;
    }


    var Ref = new Firebase("https://library-qualit.firebaseio.com");
    Ref.child('books').on("child_added", function(bookSnap) {
        Ref.child('authors/' + bookSnap.val().author_id).once("value", function(authorSnap) {
            var completeBook = bookSnap.val();
            completeBook.id = bookSnap.key();
            completeBook.author = authorSnap.val();
            $scope.books.push(completeBook);
            $scope.$apply();
        });
    });


    $scope.addBook = function() {
        var booksRef = new Firebase("https://library-qualit.firebaseio.com/books/");
        var bookRef = booksRef.push();
        var NewB = { 'title': $scope.book.title, 'description': $scope.book.description, 'author_id': $scope.book.author };
        bookRef.set(NewB);
        $scope.book.title = '';
        $scope.book.description = '';
        $scope.book.author = '';
        $scope.addBookPanel = false;
    }



    $scope.deleteBook = function(book) {
        Book.remove(book, function(bookDelete) {
            $scope.books.splice($scope.books.indexOf(bookDelete), 1);
        });
    }



});
