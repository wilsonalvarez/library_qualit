'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
    .controller('LoginCtrl', function($scope, $location, $state, userSession) {

        $scope.submit = function() {
            $state.go('dashboard');

            userSession.name = $scope.name;
            return false;
        }

    });
